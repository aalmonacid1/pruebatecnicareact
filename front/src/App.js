import React, { useEffect, useState } from 'react'
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Banner from './components/Banner';
import Swal from 'sweetalert2'
import './css/style.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
const App = () => {
  // -------------------------------------------------
  // DO NOT USE THE CODE BELOW FROM LINES 8 TO 18. THIS IS
  // HERE TO MAKE SURE THAT THE EXPRESS SERVER IS RUNNING
  // CORRECTLY. DELETE CODE WHEN COMPLETING YOUR TEST.
  const [response, setResponse] = useState([])

  // call server to see if its running
  useEffect(() => {
    const getApiResponse = () => {
      fetch('http://localhost:5000/api/products')
        .then((res) => res.json())
        .then((res) => setResponse(res))
    }
    getApiResponse()
  }, [])
  // -------------------------------------------------
  const successAlert = (_id, name, image, description, brand, category, price, countInStock, rating, numReviews) => { 
    Swal.fire({
      title: name,
      html:`
      <img class="imgProductModal" src=${'http://localhost:5000'+image}> 
      <br>
      <div class="infoProducto">
      <div class="groupDataModal">
          <p class="productPrice">$${price}</p>
          <p>Rating: ${rating} / 5</p>
      </div>
      <div class="groupDataModal">
          <p>Category: ${category}</p>
          <p>Brand: ${brand}</p>
      </div>
      <div class="descriptionModal">
        <p>${description}</p>
      </div>
      <div class="footerModal">
      <p>Stock: ${countInStock}</p>
      <p>Views: ${numReviews}</p>
      </div>
      </div>
      `,
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        'Add to cart',
      cancelButtonText:
        'Buy now',
      cancelButtonAriaLabel: 'Thumbs down'
    })
}

  return (
    <>
    <Router>
      <Navbar/>
    </Router>
    <Banner/>
    <div className="container">
     {response.map((item,index) => (
       <div key={index} className="card">
         <img className="imgProduct" src={'http://localhost:5000'+item.image}/>
        <h2>{item.name}</h2>
        <div className="groupData">
          <p className="productPrice">${item.price}</p>
          <p>Valorización: {item.rating}</p>
        </div>
        <div className="groupButton">
          <button onClick={() => successAlert(item._id, item.name, item.image, item.description, item.brand, item.category, item.price, item.countInStock, item.rating, item.numReviews)}>Ver Producto</button>
          <button>Añadir al Carrito</button>
        </div>
       </div>
      ))}
    </div>
    <Footer/>
    </>
  )
}

export default App
