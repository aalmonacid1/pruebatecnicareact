import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function Navbar() {
    return (
        <div className="navBar">
            <Link className="urlRouter" to="/">Home</Link>
            <Link className="urlRouter" to="/contacto">Contacto</Link>
        </div>
        
    )
}

export default Navbar
