import React, { useEffect, useState } from 'react';
import Footer from './Footer';
import Banner from './Banner';
const Home = () => {
    const [response, setResponse] = useState([])
    
  // call server to see if its running
  useEffect(() => {
    const getApiResponse = () => {
      fetch('http://localhost:5000/api/products')
        .then((res) => res.json())
        .then((res) => setResponse(res))
    }
    getApiResponse()
  }, [])
  // -------------------------------------------------
  return (
    <>
    <Banner/>
    <div className="container">
     {response.map((item,index) => (
       <div key={index} className="card">
         <img className="imgProduct" src={'http://localhost:5000/'+item.image}/>
        <h2>{item.name}</h2>
        <div className="groupData">
          <p className="productPrice">${item.price}</p>
          <p>Valorización: {item.rating}</p>
        </div>
        <div className="groupButton">
          <button>Ver Producto</button>
          <button>Añadir al Carrito</button>
        </div>
       </div>
      ))}
    </div>
    <Footer/>
    </>
  )
}

export default Home
